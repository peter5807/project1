//
//  MainViewController.swift
//  project1
//
//  Created by Shao on 2021/4/24.
//  Copyright © 2021 peter. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var backgroundButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI() {

        backgroundButton.backgroundColor = .orange
        titleLabel.textColor = .blue
        titleLabel.text = "標題"
        
    }

    @IBAction func pressedButton(_ sender: Any) {
        if view.backgroundColor == .black {
            view.backgroundColor = .white
            titleLabel.textColor = .blue
        } else {
            view.backgroundColor = .black
            titleLabel.textColor = .yellow

        }
    
    }

}
